package com.alberovalley.sevilladirecto.comm;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.alberovalley.sevilladirecto.comm.dto.ArticleDto;
import com.alberovalley.utils.aLog;

public class OldFeedParser {

    public static ArrayList<ArticleDto> parseArticleList(String xml) throws XPathException {

        ArrayList<ArticleDto> articles = new ArrayList<ArticleDto>();
        XPath xpath = XPathFactory.newInstance().newXPath();
        String expression = "//item";
        /*
         * XPathExpression expr = null;
         * expr = xpath.compile(expression);
         */

        InputSource inputSource = new InputSource(new StringReader(xml));
        NodeList nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
        // now nodes contains every "item" node
        aLog.d("FeedParser parseArticleList nº nodos: " + nodes.getLength());
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            NodeList items = node.getChildNodes();
            ArticleDto article = new ArticleDto();
            for (int j = 0; j < items.getLength(); j++) {
                Node item = items.item(j);
                String nodeName = item.getNodeName();
                if (!nodeName.equalsIgnoreCase("#text")) {
                    String nodeValue = item.getChildNodes().item(0).getNodeValue();
                    aLog.d("FeedParser parseArticleList node[" + j + "] name = '" + nodeName + "' ");
                    if (nodeName.equalsIgnoreCase("title")) {
                        aLog.d("FeedParser parseArticleList title: " + nodeValue);
                        article.setTitle(nodeValue);
                    } else if (nodeName.equalsIgnoreCase("link")) {
                        article.setLink(nodeValue);
                    } else if (nodeName.equalsIgnoreCase("pubDate")) {
                        article.setDate(nodeValue);
                    } else if (nodeName.equalsIgnoreCase("category")) {
                        article.addCategory(nodeValue);
                    } else if (nodeName.equalsIgnoreCase("content:encoded")) {
                        aLog.d("FeedParser parseArticleList content: " + nodeValue);
                        article.setContent(nodeValue);
                    } else if (nodeName.equalsIgnoreCase("description")) {
                        article.setDescription(nodeValue);
                    }
                    // aLog.d("FeedParser parseArticleList node[" + j + "] name = '" + nodeName + "' value: " + nodeValue);
                }
            }
            articles.add(article);
        }

        return articles;
    }
}
