package com.alberovalley.sevilladirecto.comm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.xml.xpath.XPathException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;

import com.alberovalley.sevilladirecto.comm.dto.ArticleDto;
import com.alberovalley.sevilladirecto.comm.dto.ResponseDto;
import com.alberovalley.utils.aLog;

public class FeedRetriever extends AsyncTask<FeedRetrieverParams, Void, ResponseDto> {

    private FeedListener listener;

    public void setFeedListener(FeedListener lis) {
        this.listener = lis;
    }

    @Override
    protected ResponseDto doInBackground(FeedRetrieverParams... params) {
        String feedString = "";

        ResponseDto response = new ResponseDto();
        aLog.d("FeedRetriever doInBackground: ");
        if (params.length == 1) {
            FeedRetrieverParams param = params[0];

            HttpClient httpClient = new DefaultHttpClient();
            StringBuilder builder = new StringBuilder();
            HttpGet request = new HttpGet(param.getFeedUrl());
            HttpResponse httpResponse;
            try {
                httpResponse = httpClient.execute(request);
                StatusLine statusLine = httpResponse.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                aLog.d("FeedRetriever doInBackground statusCode: " + statusCode);
                if (statusCode == 200) {
                    /*
                     * Si todo fue ok, montamos la String con los datos en formato JSON
                     */
                    HttpEntity entity = httpResponse.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                    feedString = builder.toString();
                    aLog.d("FeedRetriever doInBackground resultado del feed: " + feedString);
                    // TODO parsear el feed para crear el objeto artículo
                    ArrayList<ArticleDto> articles = OldFeedParser.parseArticleList(feedString);
                    response.setArticles(articles);
                    response.setStatusCode(ResponseCodes.CODE_OK);
                }
            } catch (ClientProtocolException e) {
                response.setStatusCode(ResponseCodes.CODE_CLIENT_PROTOCOL_EXCEPTION);
                response.setErrMessage(e.getMessage());
                aLog.e("FeedRetriever doInBackground ClientProtocolException", e);
            } catch (IOException e) {
                response.setStatusCode(ResponseCodes.CODE_IO_EXCEPTION);
                response.setErrMessage(e.getMessage());
                aLog.e("FeedRetriever doInBackground IOException", e);
            } catch (XPathException e) {
                response.setStatusCode(ResponseCodes.CODE_XPATH_EXCEPTION);
                response.setErrMessage(e.getMessage());
                aLog.e("FeedRetriever doInBackground XPathException", e);
            }

        } else {
            response.setStatusCode(ResponseCodes.CODE_PARAMS_WRONG_NUMBER);
        }

        return response;
    }

    @Override
    protected void onPostExecute(ResponseDto result) {
        if (result != null)
            aLog.v("FeedRetriever.onPostExecute result NO es nulo! " + result.getStatusCode());
        super.onPostExecute(result);
        listener.onFeedReceived(result);
    }

}
