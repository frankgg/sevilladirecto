package com.alberovalley.sevilladirecto.comm;

import com.alberovalley.sevilladirecto.comm.dto.ResponseDto;

public interface FeedListener {
	public void onFeedReceived(ResponseDto response);
}
