package com.alberovalley.sevilladirecto.comm.dto;

import java.util.ArrayList;

public class ResponseDto {
	private ArrayList<ArticleDto> articles;
	private String errMessage = "";
	private int statusCode = 0;
	public ArrayList<ArticleDto> getArticles() {
		return articles;
	}
	public void setArticles(ArrayList<ArticleDto> articles) {
		this.articles = articles;
	}
	public ArticleDto getArticle(int index) {
		return articles.get(index);
	}
	public void addArticle(ArticleDto article) {
		this.articles.add( article );
	}
	public String getErrMessage() {
		return errMessage;
	}
	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	
	
}
