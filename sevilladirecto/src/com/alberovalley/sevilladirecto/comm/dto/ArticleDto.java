package com.alberovalley.sevilladirecto.comm.dto;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class ArticleDto implements Parcelable {
    private String title;
    private String link;
    private ArrayList<String> categories;
    private String date;
    private String content;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArticleDto() {
        categories = new ArrayList<String>();
    }

    public void addCategory(String category) {
        categories.add(category);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    // ============================================================================================
    // PARCELABLE METHODS
    // ============================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(title);
        dest.writeString(link);
        dest.writeStringList(categories);
        dest.writeString(date);
        dest.writeString(content);
        dest.writeString(description);

    }

    public static final Parcelable.Creator<ArticleDto> CREATOR = new Parcelable.Creator<ArticleDto>() {
        public ArticleDto createFromParcel(Parcel in) {
            return new ArticleDto(in);
        }

        public ArticleDto[] newArray(int size) {
            return new ArticleDto[size];
        }
    };

    protected ArticleDto(Parcel in) {

        title = in.readString();
        link = in.readString();
        categories = new ArrayList<String>();
        in.readStringList(categories);
        date = in.readString();
        content = in.readString();
        description = in.readString();

    }
}
