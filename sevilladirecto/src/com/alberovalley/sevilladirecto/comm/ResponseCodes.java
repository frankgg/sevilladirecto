package com.alberovalley.sevilladirecto.comm;

public class ResponseCodes {

    public static final int CODE_OK = 1;
    public static final int CODE_PARAMS_WRONG_NUMBER = 900;
    public static final int CODE_PARAMS_TOO_MANY = 901;
    public static final int CODE_PARAMS_MISSING = 902;

    public static final int CODE_IO_EXCEPTION = 401;
    public static final int CODE_CLIENT_PROTOCOL_EXCEPTION = 402;
    public static final int CODE_XPATH_EXCEPTION = 403;

}
