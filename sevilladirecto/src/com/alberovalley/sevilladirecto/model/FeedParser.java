package com.alberovalley.sevilladirecto.model;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.alberovalley.utils.aLog;

public class FeedParser {

    public static ListFeedItems parseArticleList(String xml) throws XPathException {

        ListFeedItems articles = new ListFeedItems();
        XPath xpath = XPathFactory.newInstance().newXPath();
        String expression = "//item";
        /*
         * XPathExpression expr = null;
         * expr = xpath.compile(expression);
         */

        InputSource inputSource = new InputSource(new StringReader(xml));
        NodeList nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
        // now nodes contains every "item" node
        aLog.d("FeedParser parseArticleList nº nodos: " + nodes.getLength());
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            NodeList items = node.getChildNodes();
            FeedItem article = new FeedItem();
            for (int j = 0; j < items.getLength(); j++) {
                Node item = items.item(j);
                String nodeName = item.getNodeName();
                aLog.d("FeedParser parseArticleList node[" + i + "/" + j + "] name = '" + nodeName + "' ");
                if (!nodeName.equalsIgnoreCase("#text")) {
                    NodeList itemChildNodes = item.getChildNodes();
                    if (itemChildNodes != null && itemChildNodes.getLength() > 0) {
                        String nodeValue = item.getChildNodes().item(0).getNodeValue();

                        if (nodeName.equalsIgnoreCase("title")) {
                            aLog.d("FeedParser parseArticleList title: " + nodeValue);
                            article.setTitle(nodeValue);
                        } else if (nodeName.equalsIgnoreCase("link")) {
                            article.setLink(nodeValue);
                        } else if (nodeName.equalsIgnoreCase("pubDate")) {
                            article.setDate(nodeValue);
                        } else if (nodeName.equalsIgnoreCase("category")) {
                            article.addCategory(nodeValue);
                        } else if (nodeName.equalsIgnoreCase("content:encoded")) {
                            aLog.d("FeedParser parseArticleList content: " + nodeValue);
                            article.setContent(nodeValue);
                        } else if (nodeName.equalsIgnoreCase("description")) {
                            article.setDescription(nodeValue);
                        }
                        // aLog.d("FeedParser parseArticleList node[" + j + "] name = '" + nodeName + "' value: " + nodeValue);
                    }
                } else {
                    aLog.d("FeedParser parseArticleList node[" + i + "/" + j + "] name = '" + nodeName + "' null or childLess");
                }
            }
            articles.add(article);
        }

        return articles;
    }

    public static FeedItem parseArticle(String html)
            throws XPathExpressionException, SAXException, IOException, ParserConfigurationException {
        aLog.d("FeedParser parseArticle start");
        FeedItem staticPage = new FeedItem();

        // Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(html)));

        // XPath xpath = XPathFactory.newInstance().newXPath();
        // pattern: <div id="search" class="clerfix">
        String expression = "//div[@class=\"entry clearfix\"]";
        String start = "<div class=\"entry clearfix\">";
        String end = "</table>";
        aLog.d("FeedParser parseArticle pre part ");
        String part = html.substring(html.indexOf(start) + start.length());
        aLog.d("FeedParser parseArticle part " + part);
        String question = part.substring(0, part.indexOf(end) + end.length());
        aLog.d("FeedParser parseArticle question: " + question);

        // InputSource inputSource = new InputSource(new StringReader(html));
        // Node node = (Node) xpath.evaluate(expression, inputSource, XPathConstants.NODE);
        // NodeList nodes = (NodeList) xpath.evaluate(expression, doc, XPathConstants.NODESET);

        // now nodes contains every "item" node
        // aLog.d("FeedParser parseArticle nº nodos: " + nodes.getLength());
        /*
         * for (int i = 0; i < nodes.getLength(); i++) {
         * Node node = nodes.item(i);
         * NodeList items = node.getChildNodes();
         * FeedItem article = new FeedItem();/*
         * for (int j = 0; j < items.getLength(); j++) {
         * Node item = items.item(j);
         * String nodeName = item.getNodeName();
         * aLog.d("FeedParser parseArticleList node[" + i + "/" + j + "] name = '" + nodeName + "' ");
         * if (!nodeName.equalsIgnoreCase("#text")) {
         * NodeList itemChildNodes = item.getChildNodes();
         * if (itemChildNodes != null && itemChildNodes.getLength() > 0) {
         * String nodeValue = item.getChildNodes().item(0).getNodeValue();
         * 
         * if (nodeName.equalsIgnoreCase("title")) {
         * aLog.d("FeedParser parseArticleList title: " + nodeValue);
         * article.setTitle(nodeValue);
         * } else if (nodeName.equalsIgnoreCase("link")) {
         * article.setLink(nodeValue);
         * } else if (nodeName.equalsIgnoreCase("pubDate")) {
         * article.setDate(nodeValue);
         * } else if (nodeName.equalsIgnoreCase("category")) {
         * article.addCategory(nodeValue);
         * } else if (nodeName.equalsIgnoreCase("content:encoded")) {
         * aLog.d("FeedParser parseArticleList content: " + nodeValue);
         * article.setContent(nodeValue);
         * } else if (nodeName.equalsIgnoreCase("description")) {
         * article.setDescription(nodeValue);
         * }
         * // aLog.d("FeedParser parseArticleList node[" + j + "] name = '" + nodeName + "' value: " + nodeValue);
         * }
         * } else {
         * aLog.d("FeedParser parseArticleList node[" + i + "/" + j + "] name = '" + nodeName + "' null or childLess");
         * }
         * }
         * articles.add(article);
         * }
         */

        return new FeedItem();
    }

}
