package com.alberovalley.sevilladirecto.model;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import android.os.Parcel;
import android.os.Parcelable;

import com.alberovalley.utils.aLog;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedItem implements Parcelable {

    private String title;
    private String link;
    private ArrayList<String> categories;
    private String date;
    private String content;
    private String description;
    public boolean hasPic;
    public boolean hasYoutube;
    private String firstPicUrl;
    private String youtubeUrl;

    public String getFirstPicUrl() {
        return firstPicUrl;
    }

    public void setFirstPicUrl(String firstPicUrl) {
        this.firstPicUrl = firstPicUrl;
    }

    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FeedItem() {
        categories = new ArrayList<String>();
        title = "";
        link = "";
        date = "";
        content = "";
        description = "";
        firstPicUrl = "";
        youtubeUrl = "";
        hasPic = false;
        hasYoutube = false;
    }

    public void addCategory(String category) {
        categories.add(category);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
        try {
            final String youtubeEmbedUrl = "http://www.youtube.com/embed/";
            String videoId = content.substring(content.indexOf(youtubeEmbedUrl) + youtubeEmbedUrl.length(), content.indexOf("?rel=0"));
            if (!videoId.equalsIgnoreCase("")) {
                hasYoutube = true;
                youtubeUrl = "http://www.youtube.com/watch?v=" + videoId;
            }
        } catch (StringIndexOutOfBoundsException e) {
            // no youtube Url
        }
        try {
            final String imgTag = "<img";
            final String imgUrlStart = "src=\"";
            String firstImgTag = content.substring(content.indexOf(imgTag)); // first "<img" occurrence
            String picUrl =
                    firstImgTag.substring(firstImgTag.indexOf(imgUrlStart) + imgUrlStart.length(), content.indexOf("jpg"));
            if (!picUrl.equalsIgnoreCase("")) {
                hasPic = true;
                firstPicUrl = picUrl;
            }
            aLog.d("FeedItem.setContent hasPic " + hasPic + " picURL= " + picUrl);
        } catch (StringIndexOutOfBoundsException e) {
            // no pic Url
        }
    }

    // ============================================================================================
    // PARCELABLE METHODS
    // ============================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(title);
        dest.writeString(link);
        dest.writeStringList(categories);
        dest.writeString(date);
        dest.writeString(content);
        dest.writeString(description);
        dest.writeInt(hasPic ? 1 : 0);
        dest.writeInt(hasYoutube ? 1 : 0);
        dest.writeString(firstPicUrl);
        dest.writeString(youtubeUrl);

    }

    public static final Parcelable.Creator<FeedItem> CREATOR = new Parcelable.Creator<FeedItem>() {
        public FeedItem createFromParcel(Parcel in) {
            return new FeedItem(in);
        }

        public FeedItem[] newArray(int size) {
            return new FeedItem[size];
        }
    };

    protected FeedItem(Parcel in) {

        title = in.readString();
        link = in.readString();
        categories = new ArrayList<String>();
        in.readStringList(categories);
        date = in.readString();
        content = in.readString();
        description = in.readString();
        hasPic = in.readInt() == 1;
        hasYoutube = in.readInt() == 1;
        firstPicUrl = in.readString();
        youtubeUrl = in.readString();

    }
}
