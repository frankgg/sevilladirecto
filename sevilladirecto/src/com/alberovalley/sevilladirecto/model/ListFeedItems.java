package com.alberovalley.sevilladirecto.model;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import android.os.Parcel;
import android.os.Parcelable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListFeedItems implements Parcelable {

    private List<FeedItem> results;

    public List<FeedItem> getResults() {
        return results;
    }

    public void setResults(List<FeedItem> results) {
        this.results = results;
    }

    public void add(FeedItem item) {
        this.results.add(item);
    }

    public FeedItem get(int index) {
        return this.results.get(index);
    }

    // ============================================================================================
    // PARCELABLE METHODS
    // ============================================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeTypedList(results);

    }

    public static final Parcelable.Creator<ListFeedItems> CREATOR = new Parcelable.Creator<ListFeedItems>() {
        public ListFeedItems createFromParcel(Parcel in) {
            return new ListFeedItems(in);
        }

        public ListFeedItems[] newArray(int size) {
            return new ListFeedItems[size];
        }
    };

    public ListFeedItems() {
        results = new ArrayList<FeedItem>();
    }

    protected ListFeedItems(Parcel in) {
        in.readTypedList(results, FeedItem.CREATOR);
    }
}
