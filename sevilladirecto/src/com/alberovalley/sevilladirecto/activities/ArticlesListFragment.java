package com.alberovalley.sevilladirecto.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.alberovalley.sevilladirecto.R;
import com.alberovalley.sevilladirecto.adapter.ArticleListAdapter;
import com.alberovalley.sevilladirecto.model.FeedItem;
import com.alberovalley.sevilladirecto.model.ListFeedItems;
import com.alberovalley.utils.aLog;
import com.octo.android.robospice.spicelist.BigBinarySpiceManager;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ArticlesListFragment extends Fragment implements // FeedListener,
        OnItemClickListener {
    private ListView itemList;
    // private ArrayAdapter<String> listAdapter;
    private ArticleListAdapter listAdapter;
    private ArrayList<FeedItem> articlesList;

    private onArticleSelectedListener listener;
    private BigBinarySpiceManager spiceManagerBinary = new BigBinarySpiceManager();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_articles_list,
                container, false);

        itemList = (ListView) view.findViewById(R.id.lv_entradas_actualidad);
        itemList.setOnItemClickListener(this);
        // listAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_expandable_list_item_1);
        // listAdapter = new ArticleListAdapter(this, spiceManagerBinary, articles)
        // itemList.setAdapter(listAdapter);

        /*
         * FeedRetriever fr = new FeedRetriever();
         * FeedRetrieverParams params = new FeedRetrieverParams();
         * params.setFeedUrl(SevillaDirectoData.FEED_CATEGORY_ACTUALIDAD);
         * 
         * fr.setFeedListener(this);
         * fr.execute(params);
         * Crouton.makeText(getActivity(), "cargando", Style.INFO);
         */
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof onArticleSelectedListener) {
            listener = (onArticleSelectedListener) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implemenet ArticlesListFragment.onArticleSelectedListener");
        }
    }

    /*
     * @Override
     * public void onFeedReceived(ResponseDto response) {
     * aLog.d("ArticlesListFragment.onFeedReceived");
     * if (response.getStatusCode() == ResponseCodes.CODE_OK) {
     * aLog.d("ArticlesListFragment.onFeedReceived CODE_OK");
     * Crouton.makeText(getActivity(), "Noticias recuperadas, cargando lista ", Style.INFO).show();
     * articlesList = response.getArticles();
     * aLog.d("ArticlesListFragment.onFeedReceived Noticias recuperadas: " + articlesList.size());
     * listAdapter.clear();
     * for (int i = 0; i < articlesList.size(); i++) {
     * listAdapter.add(articlesList.get(i).getTitle());
     * }
     * listAdapter.notifyDataSetChanged();
     * } else {
     * aLog.d("ArticlesListFragment.onFeedReceived CODE_KO " + response.getStatusCode() + " " + response.getErrMessage());
     * Crouton.makeText(getActivity(), "Error al recuperar las noticias ", Style.ALERT).show();
     * }
     * }
     */

    public void loadFeedItems(ListFeedItems list) {
        listAdapter = new ArticleListAdapter(getActivity(), spiceManagerBinary, list);
        itemList.setAdapter(listAdapter);

        articlesList = (ArrayList<FeedItem>) list.getResults();
        aLog.d("ArticlesListFragment.loadFeedItems # of items: " + articlesList.size());
        /*
         * listAdapter.clear();
         * for (int i = 0; i < articlesList.size(); i++) {
         * listAdapter.add(articlesList.get(i).getTitle());
         * }
         * listAdapter.notifyDataSetChanged();
         */
        aLog.d("ArticlesListFragment.loadFeedItems list loaded ");
    }

    public void showAlert(String message) {
        if ((message + "").equalsIgnoreCase(""))
            message = "Problema con la descarga de noticias ";
        Crouton.makeText(getActivity(), message, Style.ALERT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FeedItem chosenArticle = articlesList.get(position);

        listener.onArticleSelected(chosenArticle);

    }

    public interface onArticleSelectedListener {
        public void onArticleSelected(FeedItem chosenArticle);
    }
}
