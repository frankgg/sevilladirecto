package com.alberovalley.sevilladirecto.activities;

import android.os.Bundle;

import com.alberovalley.sevilladirecto.R;
import com.alberovalley.sevilladirecto.model.FeedItem;

public class ArticleActivity extends BaseSpiceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        Bundle extras = getIntent().getExtras();
        // ArticleDto article = (ArticleDto) extras.get(Main.KEY_ARTICLE);
        FeedItem article = (FeedItem) extras.get(Main.KEY_ARTICLE);
        ArticleFragment af = (ArticleFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_article);
        af.loadArticle(article);
    }

}
