package com.alberovalley.sevilladirecto.activities;

import android.content.Intent;
import android.os.Bundle;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.alberovalley.sevilladirecto.R;
import com.alberovalley.sevilladirecto.expandable.ExpandableMenu;
import com.alberovalley.utils.aLog;

public class BaseFragmentActivity extends SherlockFragmentActivity {

    protected ActionBar bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bar = getSupportActionBar();
        bar.setHomeButtonEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            Intent intent = new Intent(this, ExpandableMenu.class);
            // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        } else if (itemId == R.id.menu_desplegable) {
            Intent intent = new Intent(this, ExpandableMenu.class);
            // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            aLog.d(getClass().getName() + "onOptionsItemSelected pulsado en menú item " + itemId);
            return super.onOptionsItemSelected(item);
        }
        return false;
    }
}
