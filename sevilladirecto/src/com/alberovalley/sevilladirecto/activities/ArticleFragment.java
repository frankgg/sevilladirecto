package com.alberovalley.sevilladirecto.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.alberovalley.sevilladirecto.R;
import com.alberovalley.sevilladirecto.model.FeedItem;
import com.alberovalley.utils.aLog;

public class ArticleFragment extends Fragment {

    private TextView tvTitle;
    private WebView wvBody;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article,
                container, false);

        wvBody = (WebView) view.findViewById(R.id.wv_article_body);
        wvBody.getSettings().setJavaScriptEnabled(true);
        wvBody.getSettings().setPluginsEnabled(true);

        tvTitle = (TextView) view.findViewById(R.id.tv_article_title);
        return view;
    }

    // public void loadArticle(ArticleDto article) {
    public void loadArticle(FeedItem article) {
        tvTitle.setText(article.getTitle());
        String content = article.getContent();
        /*
         * wvBody.setWebChromeClient(new WebChromeClient() {
         * });
         * wvBody.getSettings().setPluginState(PluginState.ON);
         */
        /*
         * final String youtubeEmbedUrl = "http://www.youtube.com/embed/";
         * try {
         * String videoId = content.substring(content.indexOf(youtubeEmbedUrl) + youtubeEmbedUrl.length(), content.indexOf("?rel=0"));
         * aLog.d("youtubeUrl? == http://www.youtube.com/watch?v=" + videoId);
         * 
         * // Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://" + videoId));
         * Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + videoId));
         * startActivity(intent);
         * // TODO diálogo que pregunte si reproducir el vídeo o no. Por ahora, vamos de tirón
         * // DialogFragment df = new
         * 
         * } catch (StringIndexOutOfBoundsException e) {
         * aLog.d("not youtube url in " + article.getTitle());
         * } catch (ActivityNotFoundException e) {
         * // TODO hacer algo si no hay con qué verlo.
         * aLog.e("can't do a thing with this url " + e.getMessage());
         * }
         */
        wvBody.loadDataWithBaseURL(null, content, "text/html", "UTF-8", null);

        aLog.d("ArticleFragment.loadArticle content: " + content);
    }
}
