package com.alberovalley.sevilladirecto.activities;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathException;

import org.xml.sax.SAXException;

import android.content.Intent;
import android.os.Bundle;

import com.alberovalley.sevilladirecto.R;
import com.alberovalley.sevilladirecto.SevillaDirectoData;
import com.alberovalley.sevilladirecto.activities.ArticlesListFragment.onArticleSelectedListener;
import com.alberovalley.sevilladirecto.expandable.ExpandableMenu;
import com.alberovalley.sevilladirecto.model.FeedItem;
import com.alberovalley.sevilladirecto.model.FeedParser;
import com.alberovalley.sevilladirecto.model.ListFeedItems;
import com.alberovalley.sevilladirecto.network.HtmlRequest;
import com.alberovalley.utils.FlashTester;
import com.alberovalley.utils.aLog;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.octo.android.robospice.request.listener.RequestProgressListener;
import com.octo.android.robospice.request.simple.SimpleTextRequest;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class Main extends BaseSpiceActivity implements onArticleSelectedListener {
    // private static final String XML_CACHE_KEY = "feed_xml";
    public final static String KEY_ARTICLE = "ARTICLE";
    private SimpleTextRequest request;

    private HtmlRequest htmlRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualidad);

        Bundle extras = getIntent().getExtras();
        String url = null;
        String menuItem = "";
        if (extras != null)
            menuItem = extras.getString(ExpandableMenu.MENU_ITEM_KEY);
        if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_BLOGUEROS)) {
            htmlRequest = new HtmlRequest(SevillaDirectoData.STATIC_BLOGUEROS);
            readStaticPage();
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_INFO_UTIL)) {
            htmlRequest = new HtmlRequest(SevillaDirectoData.STATIC_TELEFONOS_INTERES);
            readStaticPage();
        } else if (menuItem.equalsIgnoreCase("")) {
            url = SevillaDirectoData.FEED_CATEGORY_ACTUALIDAD;
            aLog.d("Main.onCreate url = " + url);
            request = new SimpleTextRequest(url);

            readFeed();
        } else {
            url = menuItem2feedUrl(menuItem);
            aLog.d("Main.onCreate url = " + url);
            request = new SimpleTextRequest(url);

            readFeed();
        }

        // readStaticPage();
        // setSupportProgressBarIndeterminate(false);
        // setSupportProgressBarVisibility(true);

        if (FlashTester.isFlashInstalled(getApplicationContext()))
            Crouton.makeText(this, "NO tiene instalado flash", Style.ALERT).show();
        else
            Crouton.makeText(this, "Sí tiene instalado flash", Style.CONFIRM).show();
    }

    public String menuItem2feedUrl(String menuItem) {
        String url = "";
        aLog.d("Main.menuItem2feedUrl menuItem " + menuItem);

        if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_PROTAS_BARRIO)) {
            url = SevillaDirectoData.FEED_CATEGORY_HIST_BARRIO;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_CANAL_TV)) {
            url = SevillaDirectoData.FEED_CATEGORY_CANAL_TV;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_ACTUALIDAD)) {
            url = SevillaDirectoData.FEED_CATEGORY_ACTUALIDAD;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_BELLAVISTA)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_BELLAVISTA;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_C_ANTIGUO)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_CASCO_ANTIGUO;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_CERRO_AMATE)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_CERRO;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_ESTE)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_ESTE;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_MACA)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_MACARENA;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_NERVION)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_NERVION;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_NORTE)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_NORTE;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_REME)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_REMEDIOS;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_SAN_PABLO)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_SAN_PABLO;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_SUR)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_SUR;
        } else if (menuItem.equalsIgnoreCase(ExpandableMenu.MENU_ITEM_DISTRITO_TRIANA)) {
            url = SevillaDirectoData.FEED_CATEGORY_DISTRITO_TRIANA;
        }
        aLog.d("Main.menuItem2feedUrl url " + url);
        return url;
    }

    @Override
    public void onArticleSelected(FeedItem chosenArticle) {
        aLog.d("Main.onArticleSelected");
        // Crouton.makeText(this, chosenArticle.getTitle(), Style.INFO).show();
        Intent intent = new Intent(getApplicationContext(), ArticleActivity.class);
        intent.putExtra(KEY_ARTICLE, chosenArticle);
        aLog.d("Main.onArticleSelected intent created");
        startActivity(intent);
    }

    public void readFeed() {
        aLog.d("Main.readFeed ");

        spiceManager.execute(request, "text", DurationInMillis.NEVER, new FeedRequestListener());
    }

    public void readStaticPage() {
        aLog.d("Main.readStaticPage ");
        /*
         * FeedRetrieverParams params = new FeedRetrieverParams();
         * params.setFeedUrl(SevillaDirectoData.STATIC_TELEFONOS_INTERES);
         */
        spiceManager.execute(htmlRequest, "text", DurationInMillis.NEVER, new StaticItemRequestListener());
        aLog.d("Main.readStaticPage htmlRequest executed");
        // binaryManager.execute(htmlRequest, "text", DurationInMillis.NEVER, new StaticItemRequestListener());

    }

    private class FeedRequestListener implements RequestListener<String>, RequestProgressListener {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            // update your UI
            ArticlesListFragment alf = (ArticlesListFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_actualidad);
            aLog.e("Main.FeedRequestListener.onRequestFailure exception " + spiceException.getMessage());
            alf.showAlert(spiceException.getMessage());
        }

        @Override
        public void onRequestSuccess(final String result) {
            aLog.d("Main.FeedRequestListener.onRequestSuccess result::> " + result);
            ListFeedItems listFeedItems;
            ArticlesListFragment alf = (ArticlesListFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_actualidad);
            try {
                listFeedItems = FeedParser.parseArticleList(result);
                // update your UI
                aLog.d("Main.FeedRequestListener.onRequestSuccess list retrieved with " + listFeedItems.getResults().size() + " items");

                alf.loadFeedItems(listFeedItems);
            } catch (XPathException e) {
                alf.showAlert("Main.FeedRequestListener.onRequestSuccess XPathException " + e.getMessage());
            }

            // setSupportProgressBarVisibility(false);
            // setSupportProgressBarIndeterminateVisibility(false);

        }

        @Override
        public void onRequestProgressUpdate(RequestProgress progress) {
            switch (progress.getStatus()) {
            case LOADING_FROM_NETWORK:
                setSupportProgressBarIndeterminate(false);
                setSupportProgress((int) (progress.getProgress() * 10000));
                break;
            default:
                break;
            }
            aLog.d("Binary progress : " + progress.getStatus() + " = " + Math.round(100 * progress.getProgress()));
        }
    }

    private class StaticItemRequestListener implements RequestListener<String> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            // update your UI
            ArticlesListFragment alf = (ArticlesListFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_actualidad);
            aLog.e("Main.StaticItemRequestListener.onRequestFailure exception " + spiceException.getMessage());
            alf.showAlert(spiceException.getMessage());
        }

        @Override
        public void onRequestSuccess(final String result) {
            aLog.d("Main.StaticItemRequestListener.onRequestSuccess result::> " + result);
            FeedItem staticItem;
            ArticlesListFragment alf = (ArticlesListFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_actualidad);
            try {
                staticItem = FeedParser.parseArticle(result);
                aLog.d("Main.StaticItemRequestListener.onRequestSuccess ITEM retrieved " + staticItem.getContent());
                // update your UI
                // aLog.d("Main.FeedRequestListener.onRequestSuccess list retrieved with " + staticItem.getResults().size() + " items");

                // alf.loadFeedItems(listFeedItems);
            } catch (XPathException e) {
                alf.showAlert("Main.StaticItemRequestListener.onRequestSuccess XPathException " + e.getMessage());
                aLog.e("Main.StaticItemRequestListener.onRequestSuccess XPathException " + e.getMessage());
            } catch (SAXException e) {
                alf.showAlert("Main.StaticItemRequestListener.onRequestSuccess SAXException " + e.getMessage());
                aLog.e("Main.StaticItemRequestListener.onRequestSuccess SAXException " + e.getMessage());
            } catch (IOException e) {
                alf.showAlert("Main.StaticItemRequestListener.onRequestSuccess IOException " + e.getMessage());
                aLog.e("Main.StaticItemRequestListener.onRequestSuccess IOException " + e.getMessage());
            } catch (ParserConfigurationException e) {
                alf.showAlert("Main.StaticItemRequestListener.onRequestSuccess ParserConfigurationException " + e.getMessage());
                aLog.e("Main.StaticItemRequestListener.onRequestSuccess ParserConfigurationException " + e.getMessage());
            }

        }
    }
}
