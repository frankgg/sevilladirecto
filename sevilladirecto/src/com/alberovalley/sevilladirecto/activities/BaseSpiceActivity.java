package com.alberovalley.sevilladirecto.activities;

import com.alberovalley.sevilladirecto.network.SevillaDirectoSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.spicelist.BigBinarySpiceManager;

public class BaseSpiceActivity extends BaseFragmentActivity {
    protected SpiceManager spiceManager = new SpiceManager(SevillaDirectoSpringAndroidSpiceService.class);
    protected BigBinarySpiceManager binaryManager = new BigBinarySpiceManager();

    @Override
    protected void onStart() {
        super.onStart();
        spiceManager.start(this);
        // binaryManager.start(this);
    }

    @Override
    protected void onStop() {
        if (spiceManager.isStarted())
            spiceManager.shouldStop();

        /*
         * if (binaryManager.isStarted())
         * binaryManager.shouldStop();
         */
        super.onStop();
    }

}
