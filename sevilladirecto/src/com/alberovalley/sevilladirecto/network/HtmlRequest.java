package com.alberovalley.sevilladirecto.network;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.alberovalley.utils.aLog;
import com.octo.android.robospice.request.SpiceRequest;

public class HtmlRequest extends SpiceRequest<String> {

    private static String reqUrl;

    public HtmlRequest(String url) {
        super(String.class);
        reqUrl = url;
        aLog.d("HtmlRequest.HtmlRequest url : " + url);
    }

    @Override
    public String loadDataFromNetwork() throws Exception {
        String html = "";
        /*
         * if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
         * System.setProperty("http.keepAlive", "false");
         * }
         */

        HttpClient httpClient = new DefaultHttpClient();
        StringBuilder builder = new StringBuilder();
        HttpGet get = new HttpGet(reqUrl);
        HttpResponse response = httpClient.execute(get);
        StatusLine statusLine = response.getStatusLine();
        int statusCode = statusLine.getStatusCode();
        aLog.d("HtmlRequest.loadDataFromNetwork statusCode : " + statusCode);
        if (statusCode == 200) {
            /*
             * Si todo fue ok, montamos la String con los datos en formato JSON
             */
            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = reader.readLine()) != null) {
                aLog.d("HtmlRequest.loadDataFromNetwork line : " + line);
                builder.append(line);
            }
            html = builder.toString();
            aLog.d("HtmlRequest.loadDataFromNetwork html : " + html);
        }
        /*
         * HttpURLConnection urlConnection = (HttpURLConnection) new URL(reqUrl)
         * .openConnection();
         * urlConnection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,* /*;q=0.8");
         * urlConnection.setRequestProperty("Content-Type", "text/html");
         * String result = IOUtils.toString(urlConnection.getInputStream());
         * urlConnection.disconnect();
         */
        httpClient.getConnectionManager().shutdown();
        return html;
    }

}
