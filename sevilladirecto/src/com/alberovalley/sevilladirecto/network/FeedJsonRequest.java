package com.alberovalley.sevilladirecto.network;

import com.alberovalley.sevilladirecto.comm.FeedRetrieverParams;
import com.alberovalley.sevilladirecto.model.ListFeedItems;
import com.alberovalley.utils.aLog;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

public class FeedJsonRequest extends SpringAndroidSpiceRequest<ListFeedItems> {

    private FeedRetrieverParams param;

    public FeedJsonRequest() {
        super(ListFeedItems.class);
    }

    public FeedJsonRequest(FeedRetrieverParams params) {
        super(ListFeedItems.class);
        this.param = params;
    }

    @Override
    public ListFeedItems loadDataFromNetwork() throws Exception {
        String url = param.getFeedUrl();
        aLog.d("FeedJsonRequest loadDataFromNetwork url=" + url);
        if (url.equalsIgnoreCase("") || url == null) {
            throw new Exception("feeds url missing ");
        }
        return getRestTemplate().getForObject(
                url
                , ListFeedItems.class);
    }
}
