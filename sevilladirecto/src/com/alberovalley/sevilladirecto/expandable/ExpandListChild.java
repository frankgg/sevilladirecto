package com.alberovalley.sevilladirecto.expandable;

public class ExpandListChild {

    private String Name;
    private String Tag;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTag() {
        return Tag;
    }

    public void setTag(String tag) {
        Tag = tag;
    }

}
