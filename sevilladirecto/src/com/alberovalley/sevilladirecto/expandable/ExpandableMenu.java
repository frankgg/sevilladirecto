package com.alberovalley.sevilladirecto.expandable;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;

import com.alberovalley.sevilladirecto.R;
import com.alberovalley.sevilladirecto.activities.ArticleActivity;
import com.alberovalley.sevilladirecto.activities.Main;
import com.alberovalley.utils.aLog;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ExpandableMenu extends Activity implements OnGroupClickListener, OnChildClickListener {

    private ExpandListAdapter ExpAdapter;
    private ArrayList<ExpandListGroup> ExpListItems;
    private ExpandableListView ExpandList;

    public static final String MENU_ITEM_KEY = "sección";

    public static final String MENU_ITEM_ACTUALIDAD = "Actualidad";
    public static final String MENU_ITEM_PROTAS_BARRIO = "Protagonistas del barrio";
    public static final String MENU_ITEM_CANAL_TV = "Canal Tv";

    public static final String MENU_ITEM_DISTRITO_BELLAVISTA = "Bellavista - La Palmera";
    public static final String MENU_ITEM_DISTRITO_C_ANTIGUO = "Casco Antiguo";
    public static final String MENU_ITEM_DISTRITO_CERRO_AMATE = "Cerro - Amate";
    public static final String MENU_ITEM_DISTRITO_ESTE = "Sevilla Este - Alcosa - Torreblanca";
    public static final String MENU_ITEM_DISTRITO_REME = "Los Remedios";
    public static final String MENU_ITEM_DISTRITO_MACA = "Macarena";
    public static final String MENU_ITEM_DISTRITO_NERVION = "Nervión";
    public static final String MENU_ITEM_DISTRITO_NORTE = "Distrito Norte";
    public static final String MENU_ITEM_DISTRITO_SAN_PABLO = "San Pablo - Santa Justa";
    public static final String MENU_ITEM_DISTRITO_SUR = "Distrito Sur";
    public static final String MENU_ITEM_DISTRITO_TRIANA = "Triana";

    public static final String MENU_ITEM_INFO_UTIL = "Información útil";
    public static final String MENU_ITEM_BLOGUEROS = "Blogueros de Barrio";

    // private SlideHolder mSlideHolder;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_side_menu);

        // mSlideHolder = (SlideHolder) findViewById(R.id.slideHolder);

        ExpandList = (ExpandableListView) findViewById(R.id.ExpList);
        ExpListItems = SetStandardGroups();
        ExpAdapter = new ExpandListAdapter(getApplicationContext(), ExpListItems);
        ExpandList.setAdapter(ExpAdapter);

        ExpandList.setGroupIndicator(null);
        ExpandList.setOnGroupClickListener(this);

        ExpandList.setOnChildClickListener(this);
        // mSlideHolder.toggle();

    }

    public ArrayList<ExpandListGroup> SetStandardGroups() {

        ArrayList<ExpandListGroup> list = new ArrayList<ExpandListGroup>();
        ArrayList<ExpandListChild> list2 = new ArrayList<ExpandListChild>();
        ExpandListGroup gru1 = new ExpandListGroup();
        gru1.setName("Secciones");
        ExpandListChild ch2_1, ch2_2, ch2_3;
        ch2_1 = new ExpandListChild();
        ch2_2 = new ExpandListChild();
        ch2_3 = new ExpandListChild();

        ch2_1.setName(MENU_ITEM_ACTUALIDAD);
        ch2_1.setTag(null);
        list2.add(ch2_1);

        ch2_2.setName(MENU_ITEM_PROTAS_BARRIO);
        ch2_2.setTag(null);
        list2.add(ch2_2);

        ch2_3.setName(MENU_ITEM_CANAL_TV);
        ch2_3.setTag(null);
        list2.add(ch2_3);
        gru1.setName("Secciones");
        gru1.setItems(list2);
        ExpandListGroup gru2 = new ExpandListGroup();
        list2 = new ArrayList<ExpandListChild>();
        gru2.setName("Distritos");
        ExpandListChild ch1_1, ch1_2, ch1_3, ch1_4, ch1_5, ch1_6, ch1_7, ch1_8, ch1_9, ch1_10, ch1_11;
        ch1_1 = new ExpandListChild();
        ch1_2 = new ExpandListChild();
        ch1_3 = new ExpandListChild();
        ch1_4 = new ExpandListChild();
        ch1_5 = new ExpandListChild();
        ch1_6 = new ExpandListChild();
        ch1_7 = new ExpandListChild();
        ch1_8 = new ExpandListChild();
        ch1_9 = new ExpandListChild();
        ch1_10 = new ExpandListChild();
        ch1_11 = new ExpandListChild();

        ch1_1.setName(MENU_ITEM_DISTRITO_BELLAVISTA);
        ch1_1.setTag(null);
        list2.add(ch1_1);

        ch1_2.setName(MENU_ITEM_DISTRITO_C_ANTIGUO);
        ch1_2.setTag(null);
        list2.add(ch1_2);

        ch1_3.setName(MENU_ITEM_DISTRITO_CERRO_AMATE);
        ch1_3.setTag(null);
        list2.add(ch1_3);

        ch1_4.setName(MENU_ITEM_DISTRITO_ESTE);
        ch1_4.setTag(null);
        list2.add(ch1_4);

        ch1_5.setName(MENU_ITEM_DISTRITO_REME);
        ch1_5.setTag(null);
        list2.add(ch1_5);

        ch1_6.setName(MENU_ITEM_DISTRITO_MACA);
        ch1_6.setTag(null);
        list2.add(ch1_6);

        ch1_7.setName(MENU_ITEM_DISTRITO_NERVION);
        ch1_7.setTag(null);
        list2.add(ch1_7);

        ch1_8.setName(MENU_ITEM_DISTRITO_NORTE);
        ch1_8.setTag(null);
        list2.add(ch1_8);

        ch1_9.setName(MENU_ITEM_DISTRITO_SAN_PABLO);
        ch1_9.setTag(null);
        list2.add(ch1_9);

        ch1_10.setName(MENU_ITEM_DISTRITO_SUR);
        ch1_10.setTag(null);
        list2.add(ch1_10);

        ch1_11.setName(MENU_ITEM_DISTRITO_TRIANA);
        ch1_11.setTag(null);
        list2.add(ch1_11);
        gru2.setItems(list2);
        list.add(gru1);
        list.add(gru2);

        ExpandListGroup gru3, gru4;
        gru3 = new ExpandListGroup();

        gru3.setName(MENU_ITEM_INFO_UTIL);
        gru4 = new ExpandListGroup();

        gru4.setName(MENU_ITEM_BLOGUEROS);

        list.add(gru3);
        list.add(gru4);

        return list;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        aLog.d("ExpandableMenu.onGroupClick grupo posición: " + groupPosition);
        if (groupPosition == 3 - 1) {
            Crouton.makeText(this, "Info útil", Style.INFO).show();
            // TODO cambiar ArticleActivity por lo que corresponda
            Intent intent = new Intent(getApplicationContext(), ArticleActivity.class);
            intent.putExtra(MENU_ITEM_KEY, MENU_ITEM_INFO_UTIL);
            startActivity(intent);
        } else if (groupPosition == 4 - 1) {
            Crouton.makeText(this, "Blogueros", Style.INFO).show();
            Intent intent = new Intent(getApplicationContext(), ArticleActivity.class);
            intent.putExtra(MENU_ITEM_KEY, MENU_ITEM_BLOGUEROS);
            startActivity(intent);
        } else {
            // expandable groups
            aLog.d("ExpandableMenu.onGroupClick grupo expansible");
            return false;
        }
        return false;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

        ExpandListChild child = (ExpandListChild) ExpAdapter.getChild(groupPosition, childPosition);
        aLog.d("ExpandableMenu.onChildClick grupo posición: " + groupPosition + " Name: " + child.getName());
        Intent intent = new Intent(getApplicationContext(), Main.class);
        intent.putExtra(MENU_ITEM_KEY, child.getName());
        startActivity(intent);
        Crouton.makeText(this, child.getName(), Style.INFO).show();
        return true;
    }
}
