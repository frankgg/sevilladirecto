package com.alberovalley.sevilladirecto;

public class SevillaDirectoData {

    public final static String FEED_CATEGORY_ACTUALIDAD = "http://www.sevilladirecto.com/category/secciones/actualidad/feed/";
    public final static String FEED_CATEGORY_HIST_BARRIO = "http://www.sevilladirecto.com/category/secciones/historias-de-barrio/feed/";
    public final static String FEED_CATEGORY_CANAL_TV = "http://www.sevilladirecto.com/category/secciones/canal-tv/feed/";
    public final static String FEED_CATEGORY_DISTRITO_BELLAVISTA = "http://www.sevilladirecto.com/category/distritos/bellavista-la-palmera/feed/";
    public final static String FEED_CATEGORY_DISTRITO_CASCO_ANTIGUO = "http://www.sevilladirecto.com/category/distritos/casco-antiguo/feed/";
    public final static String FEED_CATEGORY_DISTRITO_CERRO = "http://www.sevilladirecto.com/category/distritos/cerro-amate/feed/";
    public final static String FEED_CATEGORY_DISTRITO_ESTE = "http://www.sevilladirecto.com/category/distritos/este-alcosa-torreblanca/feed/";
    public final static String FEED_CATEGORY_DISTRITO_REMEDIOS = "http://www.sevilladirecto.com/category/distritos/los-remedios/feed/";
    public final static String FEED_CATEGORY_DISTRITO_MACARENA = "http://www.sevilladirecto.com/category/distritos/macarena/feed/";
    public final static String FEED_CATEGORY_DISTRITO_NERVION = "http://www.sevilladirecto.com/category/distritos/nervion/feed/";
    public final static String FEED_CATEGORY_DISTRITO_NORTE = "http://www.sevilladirecto.com/category/distritos/norte/feed/";
    public final static String FEED_CATEGORY_DISTRITO_SAN_PABLO = "http://www.sevilladirecto.com/category/distritos/san-pablo-santa-justa/feed/";
    public final static String FEED_CATEGORY_DISTRITO_SUR = "http://www.sevilladirecto.com/category/distritos/sur/feed/";
    public final static String FEED_CATEGORY_DISTRITO_TRIANA = "http://www.sevilladirecto.com/category/distritos/triana/feed/";

    public final static String STATIC_TELEFONOS_INTERES = "http://www.sevilladirecto.com/telefonos-de-interes/";
    public final static String STATIC_BLOGUEROS = "http://www.sevilladirecto.com/blogueros-de-barrio/";

}
