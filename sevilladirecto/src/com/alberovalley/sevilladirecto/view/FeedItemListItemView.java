package com.alberovalley.sevilladirecto.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alberovalley.sevilladirecto.R;
import com.alberovalley.sevilladirecto.model.FeedItem;
import com.octo.android.robospice.spicelist.SpiceListItemView;

public class FeedItemListItemView extends RelativeLayout implements SpiceListItemView<FeedItem> {

    private TextView articleTitleTextView;
    private ImageView articlePictureImageView;
    private FeedItem article;

    public FeedItemListItemView(Context context) {
        super(context);
        inflateView(context);
    }

    private void inflateView(Context context) {
        LayoutInflater.from(context).inflate(R.layout.listitem_actualidad, this);

        this.articleTitleTextView = (TextView) this.findViewById(R.id.li_entrada);
        this.articlePictureImageView = (ImageView) this.findViewById(R.id.li_imagen);
    }

    public void updateView(FeedItem item) {
        this.article = item;
        articleTitleTextView.setText(article.getTitle());
    }

    @Override
    public FeedItem getData() {
        return this.article;
    }

    @Override
    public ImageView getImageView() {
        return articlePictureImageView;
    }

}
