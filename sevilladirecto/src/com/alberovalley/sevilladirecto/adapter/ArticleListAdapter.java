package com.alberovalley.sevilladirecto.adapter;

import java.io.File;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.alberovalley.sevilladirecto.model.FeedItem;
import com.alberovalley.sevilladirecto.model.ListFeedItems;
import com.alberovalley.sevilladirecto.view.FeedItemListItemView;
import com.alberovalley.utils.aLog;
import com.octo.android.robospice.request.simple.BigBinaryRequest;
import com.octo.android.robospice.spicelist.BigBinarySpiceManager;
import com.octo.android.robospice.spicelist.SpiceArrayAdapter;
import com.octo.android.robospice.spicelist.SpiceListItemView;

public class ArticleListAdapter extends SpiceArrayAdapter<FeedItem> {
    /*
     * ArrayList<FeedItem> articles;
     * // ArticleViewHolder viewHolder;
     * private int elegido = -1;
     */
    // private Activity mActivity;

    // private BigBinarySpiceManager spiceManagerBinary = new BigBinarySpiceManager();
    /*
     * public void setSelected(int pos) {
     * elegido = pos;
     * if (pos > -1)
     * aLog.d("ArticleListAdapter.setSelected: posicion elegida: " + pos + " " + (articles.get(pos)).getTitle());
     * else
     * aLog.d("ArticleListAdapter.setSelected: posicion elegida: " + pos + " " + (articles.get(pos)).getTitle() + " deselección");
     * }
     */

    public ArticleListAdapter(Activity mActivity, BigBinarySpiceManager spiceManagerBinary, ListFeedItems articles) {
        super(mActivity.getApplicationContext(), spiceManagerBinary, articles.getResults());
    }

    @SuppressWarnings("unchecked")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        FeedItem currentItem = getItem(position);
        if (convertView != null) {
            view = convertView;
        } else {
            view = new FeedItemListItemView(getContext());
        }
        ((FeedItemListItemView) view).updateView(currentItem);
        // this is the most important line. It will update views automatically
        // ----------------------------------------
        updateListItemViewAsynchronously(currentItem, (SpiceListItemView<FeedItem>) view);
        // ----------------------------------------
        return view;
    }

    /**
     * Improve {@link ListView} performance while scrolling<br/>
     * 
     * <a href="ttp://developer.android.com/training/improving-layouts/smooth-scrolling.html#ViewHolder">See doc</a>
     */

    static class ViewHolder {
        TextView title;
        ImageView picture;
        LinearLayout container;
    }

    @Override
    public BigBinaryRequest createRequest(FeedItem article) {
        FeedItem item = article;
        // aLog.d("ArticleListAdapter.BigBinaryRequest article has pic " + item.hasPic + ": " + item.getFirstPicUrl());
        String picUrl = "";
        File tempFile = new File(getContext().getCacheDir(), "THUMB_IMAGE_TEMP_" + item.getTitle().hashCode()); // + ".png");
        if (item.hasPic) {
            picUrl = item.getFirstPicUrl();
            aLog.d("ArticleListAdapter.BigBinaryRequest article has pic " + item.getTitle());
            aLog.d("ArticleListAdapter.BigBinaryRequest picUrl " + item.getFirstPicUrl());
            return new BigBinaryRequest(picUrl, tempFile);
        } else {
            picUrl = "";
            aLog.d("ArticleListAdapter.BigBinaryRequest article has NO pic " + item.getTitle());
        }
        return new BigBinaryRequest(picUrl, tempFile);
    }
}
