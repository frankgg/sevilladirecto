package com.alberovalley.utils;

import android.util.Log;

public class aLog {

	public static final String LOGTAG = "sevilladirecto";
	
	public static void d(String message){
		Log.d(LOGTAG, message);
	}
	public static void v(String message){
		Log.v(LOGTAG, message);
	}
	public static void i(String message){
		Log.i(LOGTAG, message);
	}
	public static void w(String message){
		Log.w(LOGTAG, message);
	}
	public static void w(String message, Throwable tr){
		Log.w(LOGTAG, message, tr);
	}
	public static void e(String message){
		Log.e(LOGTAG, message);
	}
	public static void e(String message, Throwable tr){
		Log.e(LOGTAG, message, tr);
	}
	
	public static void wtf(String message){
		Log.wtf(LOGTAG, message);
	}
	public static void wtf(String message, Throwable tr){
		Log.wtf(LOGTAG, message, tr);
	}
	
}
