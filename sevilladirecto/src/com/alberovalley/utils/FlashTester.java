package com.alberovalley.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

public class FlashTester {

    public static boolean isFlashInstalled(Context context) {
        boolean flashInstalled = false;
        try {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo ai = pm.getApplicationInfo("com.adobe.flashplayer", 0);
            if (ai != null)
                flashInstalled = true;
        } catch (NameNotFoundException e) {
            flashInstalled = false;
        }
        return flashInstalled;
    }
}
